mod complex;

use complex::{Complex, IM_NUM};

fn main() {
    let n = 1024;
    let mut input = Vec::with_capacity(n);
    for i in 0..n {
        let t = i as f32 / n as f32;
        let a = (2.0 * std::f32::consts::PI * t * 1.0).cos();
        let b = (2.0 * std::f32::consts::PI * t * 2.0).sin();
        let c = (2.0 * std::f32::consts::PI * t * 3.0).cos();
        let d = (2.0 * std::f32::consts::PI * t * 4.0).sin();
        input.push(a + b + c + d);
    }

    let now = std::time::Instant::now();
    let result_dft = dft(&input);
    let elapsed = now.elapsed();
    println!("DFT took: {} ms", elapsed.as_millis());

    let now = std::time::Instant::now();
    let result_fft = fft_in(&input);
    let elapsed = now.elapsed();
    println!("FFT took: {} ms", elapsed.as_millis());

    assert_eq!(result_fft.len(), result_dft.len());
    for i in 0..result_fft.len() {
        assert!(result_fft[i].is_close(&result_dft[i]));
    }
}

pub fn fft_in(input: &[f32]) -> Vec<Complex> {
    let input = input.into_iter().map(|i| Complex::new(*i, 0.0)).collect();
    fft(input)
}

// input is coeff representation
fn fft(input: Vec<Complex>) -> Vec<Complex> {
    // n is a power of 2
    let n = input.len();
    if n <= 1 {
        return input;
    }
    let even = fft(input.iter().cloned().step_by(2).collect());
    let odd = fft(input.iter().cloned().skip(1).step_by(2).collect());
    let mut result = vec![Complex::new(0.0, 0.0); n];
    for j in 0..n / 2 {
        let w = (IM_NUM.scale(2.0 * j as f32 * std::f32::consts::PI / n as f32)).exp();
        result[j] = &even[j] + &(&w * &odd[j]);
        result[j + n / 2] = &even[j] - &(&w * &odd[j]);
    }

    result
}

fn dft(input: &[f32]) -> Vec<Complex> {
    let n = input.len();
    let mut result = vec![Complex::new(0.0, 0.0); n];
    for f in 0..n {
        for j in 0..n {
            result[f] = &result[f]
                + &(IM_NUM.scale(2.0 * j as f32 * std::f32::consts::PI * f as f32 / n as f32))
                    .exp()
                    .scale(input[j]);
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_video() {
        let input = [5.0, 3.0, 2.0, 1.0];
        let expected = vec![
            Complex::new(11.0, 0.0),
            Complex::new(3.0, 2.0),
            Complex::new(3.0, 0.0),
            Complex::new(3.0, -2.0),
        ];
        let actual_dft = dft(&input);
        let actual_fft = fft_in(&input);

        for (i, e) in expected.iter().enumerate() {
            assert!(e.is_close(&actual_dft[i]));
            assert!(e.is_close(&actual_fft[i]));
        }
    }

    #[test]
    fn test_from_rosettacode() {
        let input = [1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0];
        let expected = vec![
            Complex::new(4.0, 0.0),
            Complex::new(1.0, 2.4142135623730950488),
            Complex::new(0.0, 0.0),
            Complex::new(1.0, 0.4142135623730950488),
            Complex::new(0.0, 0.0),
            Complex::new(1.0, -0.4142135623730950488),
            Complex::new(0.0, 0.0),
            Complex::new(1.0, -2.4142135623730950488),
        ];
        let actual_dft = dft(&input);
        let actual_fft = fft_in(&input);
        for (i, e) in expected.iter().enumerate() {
            assert!(e.is_close(&actual_dft[i]));
            assert!(e.is_close(&actual_fft[i]));
        }
    }

    #[test]
    fn test_sine_signal() {
        let n = 8;
        let mut input = Vec::with_capacity(n);
        for i in 0..n {
            let t = i as f32 / n as f32;
            let a = (2.0 * std::f32::consts::PI * t * 1.0).cos();
            let b = (2.0 * std::f32::consts::PI * t * 2.0).sin();
            let c = (2.0 * std::f32::consts::PI * t * 3.0).cos();
            input.push(a + b + c);
        }
        let expected = vec![
            Complex::new(0.0, 0.0),
            Complex::new(4.0, 0.0),
            Complex::new(0.0, 4.0),
            Complex::new(4.0, 0.0),
            Complex::new(0.0, 0.0),
            Complex::new(4.0, 0.0),
            Complex::new(0.0, -4.0),
            Complex::new(4.0, 0.0),
        ];
        let actual_dft = dft(&input);
        let actual_fft = fft_in(&input);
        for (i, e) in expected.iter().enumerate() {
            assert!(e.is_close(&actual_dft[i]));
            assert!(e.is_close(&actual_fft[i]));
        }
    }
}
