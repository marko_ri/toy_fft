pub const IM_NUM: Complex = Complex { re: 0.0, im: 1.0 };

#[derive(Clone)]
pub struct Complex {
    pub re: f32,
    pub im: f32,
}

impl Complex {
    pub fn new(re: f32, im: f32) -> Self {
        Self { re, im }
    }

    pub fn scale(&self, t: f32) -> Self {
        Self {
            re: self.re * t,
            im: self.im * t,
        }
    }

    pub fn exp(&self) -> Self {
        //  e^(a + bi) = e^a (cos(b) + i*sin(b))
        Self {
            re: self.im.cos(),
            im: self.im.sin(),
        }
        .scale(self.re.exp())
    }

    pub fn is_close(&self, other: &Complex) -> bool {
        (self.re - other.re).abs() < 1e-1 && (self.im - other.im).abs() < 1e-1
    }
}

impl std::fmt::Display for Complex {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.im < 0.0 {
            write!(f, "{:.3} - {:.3}i", self.re, -self.im)
        } else {
            write!(f, "{:.3} + {:.3}i", self.re, self.im)
        }
    }
}

impl std::ops::Add for &Complex {
    type Output = Complex;

    fn add(self, other: Self) -> Complex {
        Complex {
            re: self.re + other.re,
            im: self.im + other.im,
        }
    }
}

impl std::ops::Sub for &Complex {
    type Output = Complex;

    fn sub(self, other: Self) -> Complex {
        Complex {
            re: self.re - other.re,
            im: self.im - other.im,
        }
    }
}

impl std::ops::Mul for &Complex {
    type Output = Complex;

    fn mul(self, other: Self) -> Complex {
        Complex {
            re: self.re * other.re - self.im * other.im,
            im: self.re * other.im + self.im * other.re,
        }
    }
}
