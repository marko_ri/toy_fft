Toy Fast Fourier Transform in Rust, based on the videos:
- [Divide & Conquer: FFT](https://www.youtube.com/watch?v=iTMn0Kt18tg)
- [What is a Discrete Fourier Transform?](https://www.youtube.com/watch?v=g8RkArhtCc4)
- [The Fast Fourier Transform (FFT) - Most Ingenious Algorithm Ever](https://www.youtube.com/watch?v=h7apO7q16V0)
